package com.android.rssreader;

import android.app.Application;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class ExecutorApplication extends Application {
    private ExecutorService pool;
    @Override
     public void onCreate() {
        super.onCreate();
        pool = Executors.newFixedThreadPool(3);
    }
    ExecutorService getExecutor ()
    {
        return pool;
    }
}
