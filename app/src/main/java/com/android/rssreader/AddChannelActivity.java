package com.android.rssreader;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.*;


final public class AddChannelActivity extends Activity {
    private BroadcastReceiver br;
    private BroadcastReceiver reSubscribe;
    private static final String SAVED_TEXT = "saved_text";
    private static final String COMPLETION_STATUS = "completionStatus";
    private static final String NORMAL="normal";
    private static final String NOT_NORMAL="notNormal";
    private  SharedPreferences sPref;
    private EditText rsslink;
    private Editor ed;
    private Handler handler;
    private static final String APP_PREFERENCES = "settings";
    private static final  String JOB_NAME = "listbuild";
    private static final  String CHANNEL_ADD = "channelAdd";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_channel);
        rsslink=(EditText) findViewById(R.id.rssLink);

        Uri data=getIntent().getData();
        if (data!=null)
        {
            rsslink.setText(data.toString());
        }

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == 1) {
                    rsslink.setText((String) msg.obj);
                }
            }
        };

        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Toast.makeText(AddChannelActivity.this, getString(R.string.incorrectInput), Toast.LENGTH_SHORT).show();
            }
        };

        final String INCORRECT_INPUT = "com.android.rssreader.action.INCORRECT_INPUT";
        final IntentFilter intFilt = new IntentFilter(INCORRECT_INPUT);
        registerReceiver(br, intFilt);

        reSubscribe = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Toast.makeText(AddChannelActivity.this, getString(R.string.reSubscribe), Toast.LENGTH_SHORT).show();
            }
        };
        final String RE_SUBSCRIBE_ACTION = "com.android.rssreader.action.RE_SUBSCRIBE";
        final IntentFilter reSubscribeIntFilt = new IntentFilter(RE_SUBSCRIBE_ACTION);
        registerReceiver(reSubscribe, reSubscribeIntFilt);
    }

    @Override
    protected void onPause() {
        super.onPause();
        savePreference();
    }

    @Override
    protected void onResume() {
        super.onResume();
        extractPreference();
    }
    @Override
    protected  void onDestroy() {
        super.onDestroy();
        unregisterReceiver(br);
        unregisterReceiver(reSubscribe);
        setCompletionStatus();
    }

    @SuppressWarnings("UnusedParameters")
    final public void onClickOk(View v)
    {
        final EditText editText=(EditText) findViewById(R.id.rssLink);
        if (editText.getText().toString().equals(""))
        {
            Toast.makeText(this, getString(R.string.inputError), Toast.LENGTH_SHORT).show();
            finish();
        }else {
            startService(new Intent(AddChannelActivity.this, ChannelListBuildService.class).putExtra(JOB_NAME, CHANNEL_ADD)
                    .putExtra(CHANNEL_ADD, editText.getText().toString()));
            finish();
        }
    }

    @SuppressWarnings("UnusedParameters")
    final public void onClickCancel(View v)
    {
        finish();
    }

    private void extractPreference()
    {
        Message msg;
        sPref = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        String status=sPref.getString(COMPLETION_STATUS, "");
        if (!status.equals(NORMAL))
        {
            String savedText = sPref.getString(SAVED_TEXT, "");
            msg = handler.obtainMessage(1, 0, 0, savedText);
            handler.sendMessage(msg);
            ed = sPref.edit();
            ed.putString(COMPLETION_STATUS, NOT_NORMAL);
            ed.apply();
        }
    }

    private void setCompletionStatus ()
    {
        sPref = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        ed = sPref.edit();
        ed.putString(COMPLETION_STATUS, NORMAL);
        ed.apply();
    }
    private void savePreference()
    {
                sPref = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        ed = sPref.edit();
        ed.putString(SAVED_TEXT, rsslink.getText().toString());
        ed.putString(COMPLETION_STATUS, NOT_NORMAL);
        ed.apply();
    }
}










