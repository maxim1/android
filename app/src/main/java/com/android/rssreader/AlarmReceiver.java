package com.android.rssreader;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.preference.PreferenceManager;

import static android.content.Context.ALARM_SERVICE;

public class AlarmReceiver extends BroadcastReceiver{
    private static final String ALARM="alarm";

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {
        AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        int FROM_ALARM = 1;
        final Intent intentService=new Intent(context,NewsUpdateService.class)
                .putExtra(ALARM, FROM_ALARM);
        PendingIntent pIntent = PendingIntent.getService(context, 0, intentService, 0);
        am.cancel(pIntent);
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(context);
        String listValue= sPref.getString(context.getString(R.string.alarmList),  context.getString(R.string.alarmListDefault));
        am.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(), Long.parseLong(listValue), pIntent);
    }
}
