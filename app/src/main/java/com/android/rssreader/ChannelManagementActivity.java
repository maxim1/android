package com.android.rssreader;

import android.app.Activity;
import android.content.*;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;

@SuppressWarnings("UnusedParameters")
public final class ChannelManagementActivity extends Activity {
    private ListView lvChannel;
    private static final String UPDATE_ACTION = "com.android.rssreader.action.DATAUPDATING";
    private static final  String ALL_CHANNEL_LIST_BUILD_ACTION = "com.android.rssreader.action.ALL_CHANNEL_LIST";
    private static final String CHANNEL_NAME = "channelName";
    private static final  String JOB_NAME = "listbuild";
    private static final String ALL_CHANNEL = "allChannel";
    private static final  String HEADER = "header";
    private static final  String DELETE_CHANNEL = "delete";
    private static final int CM_DELETE_ID=1;
    private static final  String INTENT_STRING_NAME = "string";
    private ArrayList<String> list;
    private BroadcastReceiver dataUpdatingReceiver;
    private BroadcastReceiver listBuildReceiver;

    private static final  String SAVED_HEADER = "saved_header_text";
    private static final String NEWS_COMPLETION_STATUS = "headerCompletionStatus";
    private static final int NOT_NORMAL=1;
    private static final String APP_PREFERENCES = "settings";

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel_management);
        lvChannel = (ListView) findViewById(R.id.lvChannel);
        dataUpdatingReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                startService(new Intent(ChannelManagementActivity.this, ChannelListBuildService.class).putExtra(JOB_NAME,ALL_CHANNEL));
                Toast.makeText(ChannelManagementActivity.this, getString(R.string.update), Toast.LENGTH_SHORT).show();
            }
        };
        final IntentFilter intFilt = new IntentFilter(UPDATE_ACTION);
        registerReceiver(dataUpdatingReceiver, intFilt);

        listBuildReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                list = intent.getStringArrayListExtra(INTENT_STRING_NAME);
                final ArrayAdapter<String> adapter;
                adapter = new ArrayAdapter<>(ChannelManagementActivity.this, android.R.layout.simple_list_item_1, list);
                lvChannel.setAdapter(adapter);
            }
        };
        final IntentFilter listBuildFilter = new IntentFilter(ALL_CHANNEL_LIST_BUILD_ACTION);
        registerReceiver(listBuildReceiver, listBuildFilter);

        registerForContextMenu(lvChannel);
        lvChannel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent (ChannelManagementActivity.this,NewsOfTheSelectedChannelActivity.class);
                intent.putExtra(CHANNEL_NAME,list.get(position));
                startActivity(intent);
            }
        });
    }

    public void onClickChannelAdd( View v)
    {
        startActivity(new Intent (this,AddChannelActivity.class));
    }
    public void onClickrunService(View v)
    {
        startService(new Intent(this, NewsUpdateService.class));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(dataUpdatingReceiver);
        unregisterReceiver(listBuildReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        extractHeaderPreference();
        startService(new Intent(this, ChannelListBuildService.class).putExtra(JOB_NAME,ALL_CHANNEL));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem mi = menu.add(0, 1, 0, R.string.settings);
        menu.setGroupEnabled(1,true);
        mi.setIntent(new Intent(this, PrefActivity.class));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, CM_DELETE_ID, 0, R.string.chanelDel);
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item
                .getMenuInfo();
        startService(new Intent(this, ChannelListBuildService.class).putExtra(JOB_NAME,DELETE_CHANNEL)
        .putExtra(CHANNEL_NAME,list.get((int) acmi.id)));
        return true;
    }

    private void extractHeaderPreference()
    {
        SharedPreferences sPref = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        String savedText = sPref.getString(SAVED_HEADER, "");
        if (sPref.getInt(NEWS_COMPLETION_STATUS, 0)==NOT_NORMAL)
        {
            Intent intent = new Intent(ChannelManagementActivity.this, NewsActivity.class);
            intent.putExtra(HEADER, savedText);
            startActivity(intent);
        }
    }
}


