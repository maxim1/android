package com.android.rssreader;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.IBinder;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public final class ChannelListBuildService extends Service {
    private static final String LOG_TAG = "fromDB";
    private static final String ALL_CHANNEL_LIST_BUILD_ACTION = "com.android.rssreader.action.ALL_CHANNEL_LIST";
    private static final String SELECTED_CHANNEL_LIST_BUILD_ACTION = "com.android.rssreader.action.SELECTED_CHANNEL_LIST";
    private static final String JOB_NAME = "listbuild";
    private static final String ALL_CHANNEL = "allChannel";
    private static final String SELECTED_CHANNEL = "selectedChannel";
    private static final String ID = "_id";
    private static final String INTENT_STRING_NAME = "string";
    private static final String CHANNEL_NAME = "channelName";
    private static final String CHANNEL_ADD = "channelAdd";
    private static final String SELECTED_NEWS= "selectedNews";
    private final static String EMPTY_TABLE="is not recorded in the table";
    private static final String DELETE_CHANNEL = "delete";

    private static final String ACTION = "com.android.rssreader.action.INCORRECT_INPUT";
    private static final String RE_SUBSCRIBE_ACTION = "com.android.rssreader.action.RE_SUBSCRIBE";
    private static final int READ_TIMEOUT=10000;
    private static final int CONNECT_TIMEOUT=15000;
    private static final String SELECTED_NEWS_ACTION = "com.android.rssreader.action.SELECTED_NEWS_ACTION";
    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "ChannelListBuildService onStartCommand");
        ((ExecutorApplication) getApplicationContext()).getExecutor().execute(new MyRun(intent));
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    final public void onCreate() {
        super.onCreate();
        Log.d(LOG_TAG, "ChannelListBuildService onCreate");
    }

    @Override
    final public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "ChannelListBuildService onDestroy");
    }

    private final class MyRun implements Runnable {
        private final Intent intent;
        MyRun (Intent intent)
        {
            this.intent=intent;
        }
        public void run() {
            if (intent.getStringExtra(JOB_NAME).equals(ALL_CHANNEL)) {
                getListOfAllChannels();
            }
            else if (intent.getStringExtra(JOB_NAME).equals(SELECTED_CHANNEL))
            {
                getAllNewsSelectedChannel(intent);
            }
            if (intent.getStringExtra(JOB_NAME).equals(CHANNEL_ADD))
            {

                addChannel(intent.getStringExtra(CHANNEL_ADD));
            }

            if (intent.getStringExtra(JOB_NAME).equals(SELECTED_NEWS))
            {
                getSelectedNews(intent);
            }
            if (intent.getStringExtra(JOB_NAME).equals(DELETE_CHANNEL))
            {
                deleteChannel(intent);
            }
            stopSelf();
        }
    }

    private void deleteChannel(Intent intent)
    {
        final DBChannel dbChannel=new DBChannel(getApplicationContext());
        final DB dbNews=new DB(getApplicationContext());
        dbNews.open();
        dbChannel.open();
        dbChannel.delRecByName(intent.getStringExtra(CHANNEL_NAME));
        dbNews.delRecByName(intent.getStringExtra(CHANNEL_NAME));
        dbChannel.close();
        dbNews.close();
        startService(new Intent(getApplicationContext(), ChannelListBuildService.class).putExtra(JOB_NAME,ALL_CHANNEL));
    }

    private void getSelectedNews(Intent intent)
    {
        final Cursor cursor;
        final String header=intent.getStringExtra(SELECTED_NEWS);
        final DB dbNews=new DB(getApplicationContext());
        dbNews.open();
        final String[] columns = new String[]{ID, DB.COLUMN_HEADER, DB.COLUMN_ARTICLE, DB.COLUMN_LINK, DB.COLUMN_DATE};

        cursor = dbNews.selectionByHeader(columns, header);
        final ArrayList<String> list = new ArrayList<>();
        if (cursor.moveToFirst()) {
            final int headerIndex = cursor.getColumnIndex(DB.COLUMN_HEADER);
            final int articleIndex = cursor.getColumnIndex(DB.COLUMN_ARTICLE);
            final int linkIndex = cursor.getColumnIndex(DB.COLUMN_LINK);
            final int dataIndex = cursor.getColumnIndex(DB.COLUMN_DATE);
            do {
                list.add(cursor.getString(headerIndex));
                list.add(cursor.getString(articleIndex));
                list.add(cursor.getString(linkIndex));
                list.add(cursor.getString(dataIndex));
            } while (cursor.moveToNext());
        } else {
            Log.d(LOG_TAG, EMPTY_TABLE);
        }
        cursor.close();
        dbNews.close();
        Intent broadcastIntent = new Intent(SELECTED_NEWS_ACTION);
        broadcastIntent.putStringArrayListExtra(SELECTED_NEWS, list);
        sendBroadcast(broadcastIntent);
    }

    private void getAllNewsSelectedChannel(Intent intent)
    {
        final Cursor cursor;
        final DB dbNews=new DB(getApplicationContext());
        dbNews.open();
        final String channelName = intent.getStringExtra(CHANNEL_NAME);
        final String[] columns = new String[]{ID, DB.COLUMN_HEADER, DB.COLUMN_ARTICLE, DB.COLUMN_LINK};
        cursor = dbNews.selectionByChannelName(columns, channelName);
        final ArrayList<String> list = new ArrayList<>();
        if (cursor.moveToFirst()) {
            int headerIndex = cursor.getColumnIndex(DB.COLUMN_HEADER);
            do {
                list.add(0,cursor.getString(headerIndex));
            } while (cursor.moveToNext());
        }
        cursor.close();
        dbNews.close();
        Intent broadcastIntent = new Intent(SELECTED_CHANNEL_LIST_BUILD_ACTION);
        broadcastIntent.putStringArrayListExtra(INTENT_STRING_NAME, list);
        sendBroadcast(broadcastIntent);
    }


    private void getListOfAllChannels ()
    {
        final Cursor cursor;
        final DBChannel dbChannel = new DBChannel(getApplicationContext());
        dbChannel.open();
        cursor = dbChannel.getAllData();
        final ArrayList<String> list = new ArrayList<>();
        int headerIndex = cursor.getColumnIndex(DBChannel.COLUMN_CHANNEL);
        if (cursor.moveToFirst())
            do {
                if (cursor.getString(headerIndex) == null || cursor.getString(headerIndex).equals("")) {
                    continue;
                }
                list.add(cursor.getString(headerIndex));
            } while (cursor.moveToNext());
        cursor.close();
        dbChannel.close();
        Intent broadcastIntent = new Intent(ALL_CHANNEL_LIST_BUILD_ACTION);
        broadcastIntent.putStringArrayListExtra(INTENT_STRING_NAME, list);
        sendBroadcast(broadcastIntent);
    }

    private void addChannel(String UrlChannel)
    {
        try {
            final DBChannel dbChannel= new DBChannel(getApplicationContext());
            dbChannel.open();
            Cursor cursor=dbChannel.checkUrl(UrlChannel);
            if (cursor.getCount()!=0) {
                Intent reSubscribeIntent = new Intent(RE_SUBSCRIBE_ACTION);
                sendBroadcast(reSubscribeIntent);
                cursor.close();
                return;
            }
            URL url = new URL(UrlChannel);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(READ_TIMEOUT);
            conn.setConnectTimeout(CONNECT_TIMEOUT);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBChannel.COLUMN_URL, UrlChannel);
            contentValues.put(DBChannel.COLUMN_CHANNEL, "");
            dbChannel.addData(contentValues);
            cursor.close();
            dbChannel.close();
            conn.disconnect();
        } catch (IOException | RuntimeException e) {
            Intent broadcastIntent = new Intent(ACTION);
            sendBroadcast(broadcastIntent);
        }
    }
}
