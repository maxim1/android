package com.android.rssreader;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class PrefActivity extends PreferenceActivity {
    private AlarmManager am;
    private static final String ALARM="alarm";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference);
        am=(AlarmManager) getSystemService(ALARM_SERVICE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        updateDataAlarm();
    }

    private void updateDataAlarm ()
    {
        final int FROM_ALARM=1;
        final Intent intent=new Intent(this,NewsUpdateService.class)
                .putExtra(ALARM,FROM_ALARM);
        PendingIntent pIntent = PendingIntent.getService(getApplicationContext(), 0, intent, 0);
        am.cancel(pIntent);
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String listValue= sPref.getString(getString(R.string.alarmList),  getString(R.string.alarmListDefault));
        am.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(), Long.parseLong(listValue), pIntent);
    }
}




