package com.android.rssreader;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

final class DBChannel{

    private static final int DB_VERSION=1;
    private static final String DB_NAME="ChannelDB";
    private static final String TABLE_CHANNEL="channelTable";

    private static final String COLUMN_ID ="_id";
    static final String COLUMN_CHANNEL="channel";
    static final String COLUMN_URL="url";

  private static final String DB_CREATE =
            "create table " + TABLE_CHANNEL + "(" +
                    COLUMN_ID + " integer primary key," +
                    COLUMN_CHANNEL + " text," +
                    COLUMN_URL + " text" +
                    ");";
    private final Context Context;
    private DBHelper dbHelper;
    private SQLiteDatabase database;

    DBChannel(Context context) {
        this.Context = context;
    }

    void open() {
        dbHelper = new DBHelper(Context);
        try{
            database = dbHelper.getWritableDatabase();
        }catch (SQLiteCantOpenDatabaseException e)
        {
            e.printStackTrace();
        }
    }

    void close() {
        if (dbHelper !=null)
            try{
                dbHelper.close();
            }catch (Exception e)
            {
                e.printStackTrace();
            }
    }

    Cursor getAllData() {
            return database.query(TABLE_CHANNEL, null, null, null, null, null, null);
    }
    void  updateData (ContentValues contentValues,String url)
    {
        try{
            database.update(DBChannel.TABLE_CHANNEL,contentValues,DBChannel.COLUMN_URL + "= ?", new String[] {url});
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    void addData( ContentValues contentValues) {
        try{
            database.insert(TABLE_CHANNEL,null,contentValues);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    void delRecByName(String channelName) {
        try {
            database.delete(TABLE_CHANNEL, COLUMN_CHANNEL + " = ?" ,new String[] {channelName});
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    Cursor checkUrl(String url)
    {
        return database.query(TABLE_CHANNEL,null,COLUMN_URL+" = ?",new String[]{url},null,null,null);
    }

    private class DBHelper extends SQLiteOpenHelper {

        private DBHelper(Context context) {
            super(context, DBChannel.DB_NAME, null, DBChannel.DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try{
                db.execSQL(DB_CREATE);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

}
