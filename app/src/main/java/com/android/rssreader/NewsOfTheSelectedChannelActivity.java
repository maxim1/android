package com.android.rssreader;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;

public final class NewsOfTheSelectedChannelActivity extends Activity {
    private ListView lvNews;
    private ArrayList<String>list;
    private final static String INTENT_STRING_NAME = "string";
    private final static String CHANNEL_NAME = "channelName";
    private final static String JOB_NAME = "listbuild";
    private final static String SELECTED_CHANNEL = "selectedChannel";
    private final static String HEADER = "header";
    private BroadcastReceiver listBuildReceiver;
    private final static String SELECTED_CHANNEL_LIST_BUILD_ACTION = "com.android.rssreader.action.SELECTED_CHANNEL_LIST";

    @SuppressWarnings("unchecked")
    @Override
   final protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_of_the_selected_channel);
        lvNews = (ListView) findViewById(R.id.lvNews);
        String channelName=getIntent().getStringExtra(CHANNEL_NAME);
        startService(new Intent(this, ChannelListBuildService.class).putExtra(JOB_NAME,SELECTED_CHANNEL)
                .putExtra(CHANNEL_NAME,channelName));

        listBuildReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                list = intent.getStringArrayListExtra(INTENT_STRING_NAME);
                final ArrayAdapter<String> adapter;
                adapter = new ArrayAdapter<>(NewsOfTheSelectedChannelActivity.this, android.R.layout.simple_list_item_1, list);
                lvNews.setAdapter(adapter);

            }
        };
        final IntentFilter listBuildFilt = new IntentFilter(SELECTED_CHANNEL_LIST_BUILD_ACTION);
        registerReceiver(listBuildReceiver, listBuildFilt);

        lvNews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent (NewsOfTheSelectedChannelActivity.this,NewsActivity.class);
                intent.putExtra(HEADER,list.get(position));
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(listBuildReceiver);
    }
}


