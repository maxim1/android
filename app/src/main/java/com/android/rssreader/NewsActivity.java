package com.android.rssreader;

import android.app.Activity;
import android.content.*;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public final class NewsActivity extends Activity {
    private ArrayList<String>list;
    private static final int HEADER_NEWS=0;
    private static final int ARTICLE_NEWS=1;
    private static final int LINK_NEWS=2;
    private static final int DATE_NEWS =3;
    private WebView mWebView;

    private static final String SAVED_HEADER = "saved_header_text";
    private static final String NEWS_COMPLETION_STATUS = "headerCompletionStatus";
    private static final int NORMAL=0;
    private static final int NOT_NORMAL=1;
    private SharedPreferences sPref;
    private SharedPreferences.Editor ed;
    private static final String APP_PREFERENCES = "settings";
    private static final  String SELECTED_NEWS_ACTION = "com.android.rssreader.action.SELECTED_NEWS_ACTION";
    private static final  String SELECTED_NEWS= "selectedNews";
    private static final  String JOB_NAME = "listbuild";
    private final static String HEADER = "header";
    private final SimpleDateFormat formatIn = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z",Locale.US);
    private final SimpleDateFormat formatOut = new SimpleDateFormat("HH:mm:ss dd.MM.yyyy",Locale.US);
    private BroadcastReceiver selectedNewsReceiver;
    private IntentFilter listBuildFilt;
    private String header;

    @SuppressWarnings("unchecked")
    @Override
    final protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        header=getIntent().getStringExtra(HEADER);
        startService(new Intent(NewsActivity.this, ChannelListBuildService.class).putExtra(JOB_NAME, SELECTED_NEWS)
                .putExtra(SELECTED_NEWS, header));
        mWebView = (WebView) findViewById(R.id.webView);
        mWebView.setWebViewClient(new MyAppWebViewClient());

        selectedNewsReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                list = intent.getStringArrayListExtra(SELECTED_NEWS);
                Date newsDate;
                try {
                    newsDate = formatIn.parse(list.get(DATE_NEWS));
                    String htmlText = "<html><body>"
                        +"<h3>"+list.get(HEADER_NEWS)+"</h3>"
                        +list.get(ARTICLE_NEWS)
                        +"<p><a href=\""+list.get(LINK_NEWS)+"\">"+list.get(LINK_NEWS)+"</a></p>"
                        +formatOut.format(newsDate)+
                        "</body></html>";
                mWebView.loadDataWithBaseURL(null, htmlText, "text/html", "en_US", null);
                }
                 catch (ParseException e) {
                    e.printStackTrace() ;
                }
            }
        };
        listBuildFilt = new IntentFilter(SELECTED_NEWS_ACTION);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(selectedNewsReceiver, listBuildFilt);
        sPref = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        ed = sPref.edit();
        ed.putString(SAVED_HEADER, header);
        ed.putInt(NEWS_COMPLETION_STATUS, NOT_NORMAL);
        ed.apply();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        sPref = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        ed = sPref.edit();
        ed.putInt(NEWS_COMPLETION_STATUS, NORMAL);
        ed.apply();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(selectedNewsReceiver);
    }

    private class MyAppWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if(Uri.parse(url).getHost().length() == 0) {
                return false;
            }

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            view.getContext().startActivity(intent);
            return true;
        }
    }
}

