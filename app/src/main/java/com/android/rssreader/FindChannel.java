package com.android.rssreader;

import android.content.ContentValues;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

final class FindChannel {
    private  final String rssLink;
    private final DBChannel dbChannel;
    private static final int READ_TIMEOUT=10000;
    private static final int CONNECT_TIMEOUT=15000;
    FindChannel(String rssLink, DBChannel dbChannel)
    {
        this.rssLink=rssLink;
        this.dbChannel = dbChannel;
    }

    final void parseXmlFindChanel()
    {
                XmlPullParserFactory xmlFactoryObject;
                try {
                    final ContentValues contentValues=new ContentValues();
                    int flag = 0, title = 0;
                    String mTempStr = "";
                    URL url = new URL(rssLink);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setReadTimeout(READ_TIMEOUT);
                    conn.setConnectTimeout(CONNECT_TIMEOUT);
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    xmlFactoryObject = XmlPullParserFactory.newInstance();
                    XmlPullParser mParser = xmlFactoryObject.newPullParser();
                    mParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    mParser.setInput(stream, null);
                    while (mParser.getEventType() != XmlPullParser.END_DOCUMENT) {
                        switch (mParser.getEventType()) {
                            case XmlPullParser.START_DOCUMENT:
                                break;
                            case XmlPullParser.START_TAG:
                                mTempStr = mParser.getName();
                                if (mParser.getName().equals("item")) {
                                    flag = 1;
                                }
                                break;
                            case XmlPullParser.END_TAG:
                                if (mParser.getName().equals("item")) {
                                    flag = 0;
                                }
                                if (flag == 1) {
                                    mParser.next();
                                }
                                break;
                            case XmlPullParser.TEXT:
                                 if (mTempStr.equals("title") && (title == 0)) {
                                     dbChannel.open();
                                     contentValues.put(DBChannel.COLUMN_CHANNEL, mParser.getText());
                                     dbChannel.updateData(contentValues,rssLink);
                                     dbChannel.close();
                                    title++;
                                }
                                break;
                            default:
                                break;
                        }
                        mParser.next();
                    }
                    conn.disconnect();
                    stream.close();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
    }
}

