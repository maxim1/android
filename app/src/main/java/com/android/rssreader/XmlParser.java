package com.android.rssreader;

import android.content.ContentValues;
import android.database.Cursor;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

final class XmlParser {
    private  final String rssLink;
    private final DB dbNews;
    private static final int READ_TIMEOUT=10000;
    private static final int CONNECT_TIMEOUT=15000;

    XmlParser(String rssLink, DB dbNews)
    {
        this.rssLink=rssLink;
        this.dbNews=dbNews;
    }

   final void parseXml()
    {
                XmlPullParserFactory xmlFactoryObject;
                dbNews.open();
                ContentValues contentValues=new ContentValues();
                try {
                    int flag = 0, title = 0;
                    String mTempStr = "";
                    String newsUrl="";
                    URL url = new URL(rssLink);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setReadTimeout(READ_TIMEOUT);
                    conn.setConnectTimeout(CONNECT_TIMEOUT);
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    xmlFactoryObject = XmlPullParserFactory.newInstance();
                    XmlPullParser mParser = xmlFactoryObject.newPullParser();
                    mParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    mParser.setInput(stream, null);
                    while (mParser.getEventType() != XmlPullParser.END_DOCUMENT) {
                        switch (mParser.getEventType()) {
                            case XmlPullParser.START_DOCUMENT:
                                break;
                            case XmlPullParser.START_TAG:
                                mTempStr = mParser.getName();
                                if (mParser.getName().equals("item")||mParser.getName().equals("entry")) {
                                    flag = 1;
                                    }
                                    break;
                            case XmlPullParser.END_TAG:
                                if (mParser.getName().equals("item")||mParser.getName().equals("entry")) {
                                    flag = 0;
                                    Cursor cursor=dbNews.checkUrl(newsUrl);
                                    if (cursor.getCount()==0)
                                    {
                                        dbNews.addData(contentValues);
                                        newsUrl="";
                                        cursor.close();
                                    }
                                    else
                                    {
                                        cursor.close();
                                    }
                                }
                                if (flag == 1) {
                                    mParser.next();
                                }
                                break;
                            case XmlPullParser.TEXT:
                                if (flag == 1) {
                                    switch (mTempStr) {
                                        case "link":
                                            contentValues.put(DB.COLUMN_LINK, mParser.getText());
                                            newsUrl=mParser.getText();
                                            break;
                                        case "id":
                                            contentValues.put(DB.COLUMN_LINK, mParser.getText());
                                            newsUrl=mParser.getText();
                                            break;
                                        case "title":
                                            contentValues.put(DB.COLUMN_HEADER, mParser.getText());
                                            break;
                                        case "description":
                                            contentValues.put(DB.COLUMN_ARTICLE, mParser.getText());
                                            break;
                                        case "summary":
                                            contentValues.put(DB.COLUMN_ARTICLE, mParser.getText());
                                            break;
                                        case "pubDate":
                                            contentValues.put(DB.COLUMN_DATE, mParser.getText());
                                            break;
                                        case "updated":
                                            contentValues.put(DB.COLUMN_DATE, mParser.getText());
                                            break;
                                        default:
                                            break;
                                    }
                                } else if (mTempStr.equals("title") && (title == 0)) {
                                    contentValues.put(DB.COLUMN_CHANNEL, mParser.getText());
                                    title++;
                                }
                                break;
                            default:
                                break;
                        }
                        mParser.next();
                    }
                    conn.disconnect();
                    stream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dbNews.close();
    }
}


