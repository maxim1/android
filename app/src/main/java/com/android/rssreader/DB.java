package com.android.rssreader;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

 final class DB {

    private static final int DB_VERSION=1;
    private static final String DB_NAME="newsDB";
    private static final String TABLE_NEWS="newsTable";

    private static final String COLUMN_ID ="_id";
    static final String COLUMN_CHANNEL="channel";
    static final String COLUMN_HEADER="header";
    static final String COLUMN_ARTICLE="article";
    static final String COLUMN_LINK ="link";
    static final String COLUMN_DATE ="data";

    private static final String DB_CREATE =
            "create table "+TABLE_NEWS+"("+ COLUMN_ID +" integer primary key,"+
                    COLUMN_CHANNEL+" text,"+COLUMN_HEADER+" text,"+COLUMN_ARTICLE+" text,"
                    + COLUMN_LINK +" text,"+ COLUMN_DATE +")";

    private final Context Context;
    private DBHelper dbHelper;
    private SQLiteDatabase database;

    DB(Context context) {
        this.Context = context;
    }

    void open() {
        dbHelper = new DBHelper(Context);
        try{
            database = dbHelper.getWritableDatabase();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    Cursor selectionByChannelName(String[] columns,String channel)
    {
        return database.query(TABLE_NEWS,columns,COLUMN_CHANNEL+"=?",new String[]{channel},null,null,null);
    }

     Cursor selectionByHeader(String[] columns,String header)
     {
         return database.query(TABLE_NEWS,columns,COLUMN_HEADER+"=?",new String[]{header},null,null,null);
     }

     void delRecByName(String channelName) {
         try{
             database.delete(TABLE_NEWS, COLUMN_CHANNEL + " = ?" ,new String[] {channelName});
         }catch (Exception e)
         {
             e.printStackTrace();
         }
     }

     Cursor checkUrl(String url)
     {
         return database.query(TABLE_NEWS,null,COLUMN_LINK+" = ?",new String[]{url},null,null,null);
     }

    void close() {
        try{
            if (dbHelper!=null) {
                dbHelper.close();
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

     void addData( ContentValues contentValues) {
         try{
             database.insert(TABLE_NEWS,null,contentValues);
         }catch (Exception e)
         {
             e.printStackTrace();
         }
    }

     final private class DBHelper extends SQLiteOpenHelper {

        private DBHelper(Context context) {
            super(context, DB.DB_NAME, null, DB.DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(DB_CREATE);
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

}
