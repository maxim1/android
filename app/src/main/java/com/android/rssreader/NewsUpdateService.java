package com.android.rssreader;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import java.util.ArrayList;

public final class NewsUpdateService extends Service {
    private final  String LOG_TAG = "fromDB";
    private static final String UPDATE_ACTION= "com.android.rssreader.action.DATAUPDATING";
    private NotificationManager nm;
    private int intentValue;
    private static final String ALARM="alarm";

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
    @Override
    final public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "NewsUpdateService onStartCommand");
        intentValue=intent.getIntExtra(ALARM,0);
        ((ExecutorApplication) getApplicationContext()).getExecutor().execute(new MyRun());
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    final public void onCreate() {
        super.onCreate();
        Log.d(LOG_TAG, "NewsUpdateService onCreate");
        nm=(NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    final public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "NewsUpdateService onDestroy");
    }

    private final class MyRun implements Runnable {
        public void run() {
            final ArrayList<String>list= new ArrayList<>();
            final DBChannel dbChannel=new DBChannel(getApplicationContext());
            dbChannel.open();
            Cursor cursor=dbChannel.getAllData();
            if (cursor.moveToFirst())
            {
                int urlIndex=cursor.getColumnIndex(DBChannel.COLUMN_URL);
                do {
                    list.add(cursor.getString(urlIndex));
                }while (cursor.moveToNext());
            }
            cursor.close();
            dbChannel.close();
            DB dbNews=new DB(getApplicationContext());
            for (String aList : list) {
                FindChannel findChannel = new FindChannel(aList, dbChannel);
                findChannel.parseXmlFindChanel();
                XmlParser xmlParser = new XmlParser(aList, dbNews);
                xmlParser.parseXml();
            }
            Intent broadcastIntent = new Intent(UPDATE_ACTION);
            sendBroadcast(broadcastIntent);
            int FROM_ALARM = 1;
            if ( intentValue== FROM_ALARM){
                sendNotification();
            }
            stopSelf();
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void sendNotification()
    {
        Notification.Builder builder=new Notification.Builder(getApplicationContext());
        final Intent intent=new Intent(getApplicationContext(), ChannelManagementActivity.class);
        final PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),0,intent,PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.ic_launcher))
                .setTicker( getString(R.string.ticker))
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentTitle( getString(R.string.contentitle))
                .setContentText( getString(R.string.contenttext));
        Notification notification=builder.build();
        final int NOTIFICATION = 10;
        nm.notify(NOTIFICATION,notification);
    }
}
